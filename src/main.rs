#![feature(allocator_api)]
#![no_std]

#[macro_use]
extern crate alloc;
extern crate spin;

use alloc::alloc::{Alloc, AllocErr, Layout};
use core::panic::PanicInfo;
use spin::Mutex;

/// A simple allocator that allocates memory linearly and ignores freed memory 
#[derive(Debug)]
struct BumpAllocator {
    heap_start: usize,
    heap_end: usize,
    next: usize
}

impl BumpAllocator {
    pub const fn new(heap_start: usize, heap_end: usize) -> Self {
        Self { heap_start, heap_end, next: heap_start}
    }
}

unsafe impl Alloc for BumpAllocator {
    unsafe fn alloc(&mut self, layout: Layout) -> Result<*mut u8, AllocErr> {
        let alloc_start = align_up(self.next, layout.align());
        let alloc_end = alloc_start.saturating_add(layout.size());

        if alloc_end <= self.heap_end {
            self.next = alloc_end;
            Ok(alloc_start as *mut u8)
        }
        else {
            Err( AllocErr::Exhausted { request: layout })
        }
    }
    
    unsafe fn dealloc(&mut self, ptr: *mut u8, layout: Layout) {
        // TODO: Leak memory
    }
    
}
/*
pub unsafe trait Alloc {
    unsafe fn alloc(&mut self, layout: Layout) -> Result<*mut u8, AllocErr>;
    unsafe fn dealloc(&mut self, ptr: *mut u8, layout: Layout);
}*/

/// Align downwards. Return the greatest x with the alignment `align` so that x <= addr. The alignment must be a power of 2.
pub fn align_down(addr: usize, align: usize) -> usize {
    if align.is_power_of_two() {
        addr & !(align -1)
    } else if align == 0 {
        addr
    } else {
        panic!("Must be power of two");
    }
}

/// Align upwards. Return the smallest x with alignment `align` so that x >= addr. The alignment must be power of 2.
pub fn align_up(addr: usize, align: usize) -> usize {
    align_down(addr + align -1, align)
}

struct LockedBumpAllocator(Mutex<BumpAllocator>);

unsafe impl<'a> Alloc for &'a LockedBumpAllocator {
    unsafe fn alloc(&mut self, layout: Layout) -> Result<*mut u8, AllocErr> {
        self.0.lock().alloc(layout)
    }
    
    unsafe fn dealloc(&mut self, ptr: *mut u8, layout: Layout) {
        self.0.lock().dealloc(ptr, layout)
    }
}

fn main() {

}

#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    
}
